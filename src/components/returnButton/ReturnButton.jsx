const ReturnButton = () => (
  <button className='return-btn'>
    <span>Regresar</span>
    <i className='fas fa-arrow-left'></i>
  </button>
);

export default ReturnButton;
