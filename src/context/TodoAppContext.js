import { createContext } from 'react';

const TodoAppContext = createContext(null);

export default TodoAppContext;
