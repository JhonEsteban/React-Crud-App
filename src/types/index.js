export const types = {
  addTodo: 'ADD_TODO',
  updateTodo: 'UPDATE_TODO',
  deleteTodo: 'DELETE_TODO',
  toggleTodoCompleted: 'TOGGLE_TODO_COMPLETED',
  clearTodos: 'CLEAR_TODOS',
};
