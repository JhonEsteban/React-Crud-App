# React Crud App 💻💼

This project is a crud with pages per action managed with routes, the information is available globally with the Context API, the actions are executed with useReducer and everything is persistent with localStorage.

## Desktop design

![](https://i.imgur.com/QytbGly.png)
![](https://i.imgur.com/7mAdZLT.png)

## Technologies used: 📕

- [React](https://reactjs.org/ 'Click here')
- [React Router](https://reactrouter.com/web/guides/quick-start 'Click here')
- [Sass](https://sass-lang.com/ 'Click here')

## To run the project: 👇

1. Clone the repository

```
git clone https://github.com/JhonEsteban/React-Crud-App.git
```

2. Install the node modules

```
npm install
```

3. Start the dev server

```
npm start
```

### My social networks 👋🏼

- [Linkedin](https://www.linkedin.com/in/jhon-esteban-herrera-zabala-6b960b196 'My Linkendin')
- [Twitter](https://twitter.com/JhonDev_19 'My Twitter')
